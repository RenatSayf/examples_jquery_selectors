﻿(function ()
{
    'use strict';
    var app = WinJS.Application;
    var activation = Windows.ApplicationModel.Activation;
    app.onactivated = function (args)
    {
        if (args.detail.kind === activation.ActivationKind.launch)
        {
            if (args.detail.previousExecutionState !== activation.ApplicationExecutionState.terminated)
            {
                // TODO: это приложение только что запущено. Инициализируйте здесь свое приложение.
                $(document).ready(function ()
                {
                    //Селекторы базовые
                    //$('*').css('border', '3px solid black')// выбирает все блоки на странице
                    //$('p').css('border', '3px solid black')// выбирает все блоки <p/>
                    //$('span').css('border', '3px solid black')// выбирает все блоки <span/>
                    //$('#middle').css('border', '3px solid black')// выбирает блок с id="middle"
                    //$('.s1').css('border', '3px solid black')// выбирает все блоки с class="s1"

                    //Фильтры базовые
                    //$('p:odd').css('border', '3px solid black')// выбирает все нечетные блоки <p/>
                    //$('p:even').css('border', '3px solid black')// выбирает все четные блоки (0-ой элемент здесь тоже четный)<p/>
                    //$('p:eq(1)').css('border', '3px solid black')// выбирает все блоки <p/> с индексом == 1
                    //$('p:gt(1)').css('border', '3px solid black')// выбирает все блоки <p/> с индексом > 1
                    //$('p:lt(3)').css('border', '3px solid black')// выбирает все блоки <p/> с индексом < 3
                    //$(':header').css('border', '3px solid black')// выбирает все заголовки <h/> 
                    //$(':animated').css('border', '3px solid black')// выбирает все анимированные в данный момент элементы
                    //$('p:first').css('border', '3px solid black')// выбирает первый элемент <p/>
                    //$('p:last').css('border', '3px solid black')// выбирает последний элемент <p/>
                    //$('*:not(p)').css('border', '3px solid black')// выбирает все элементы не <p/>
                    //$('body *:gt(3):not(:header)').css('border', '3px solid black')

                    //Фильтры контента и видимости
                    //$('span:contains("е")').css('border', '3px solid black')// выбирает все span, внутри котрых есть текст содержащий "е"
                    //$(':empty').css('border', '3px solid black')// выбирает все пустые элементы
                    //$('#divBlock:has(p)').css('border', '3px solid black')// выбирает элемент с id="divBlock" если в нем содержится элемент <p/>
                    //$('div:parent').css('border', '3px solid black')// выбирает элементы div, имеющие дочерние элементы
                    //$(':hidden').css('border', '3px solid black')// выбирает все скрытые элементы
                    //$(':visible').css('border', '3px solid black')// выбирает все видимые элементы

                    //Фильтры атрибутов
                    //$('*[id]').css('border', '3px solid black')// выбирает все элементы у которых есть id
                    //$('*[id*=v]').css('border', '3px solid black')// выбирает все элементы у которых в id есть символ 'v'
                    //$('*[id^=fi]').css('border', '3px solid black')// выбирает все элементы у которых id начинается на "fi"
                    //$('*[id$=ck]').css('border', '3px solid black')// выбирает все элементы у которых id оканчивается на "ck"
                    //$('*[id=middle]').css('border', '3px solid black')// выбирает все элементы у которых id = "middle"
                    //$('*[id!=middle]').css('border', '3px solid black')// выбирает все элементы у которых id != "middle"

                    //Семейные фильтры
                    //$('div *:nth-child(1)').css('border', '3px solid black')// выбирает дочерний элемент с указанным индексом, первый индекс начинается с 1
                    //$('*:nth-child(even)').css('border', '3px solid black')// выбирает четные дочерние элементы
                    //$('body p:nth-child(odd)').css('border', '3px solid black')// выбирает нечетные дочерние элементы
                    //$('body p:nth-child(2n+1)').css('border', '3px solid black')// выбирает все дочерние элементы с индеком подходящим под выражение (2n+1)
                    //$('body *:first-child').css('border', '3px solid black')// выбирает первый дочерний элемент в <span/>
                    //$('span *:last-child').css('border', '3px solid black')// выбирает последний дочерний элемент в <span/>
                    //$('div *:only-child').css('border', '3px solid black')// выбирает элементы которые являются единственными дочерними элементами в <div/>

                    //Фильтрация отобранных эолементов
                    //Windows.UI.Popups.MessageDialog($('div > *').is('span') ? "Есть элемент span" : "Нет элементов span").showAsync();//проверяет есть ли в <div/> элементы <span/>
                    //$('p').slice(1, 4).css('border', '3px solid black')// выбирает элементы <p/> с индексами 1, 2 и 3
                    //$('p').filter('*[class]').css('border', '3px solid black');// выбирает все элементы <p/> с атрибутом class

                    //$('p').filter(function ()
                    //{
                    //    if (this.hasAttribute("id"))
                    //    {
                    //        return true;
                    //    } else
                    //    {
                    //        return false;
                    //    }
                    //}).css('border', '3px solid black');// выбирает все элементы <p/> имеющие атрибут "id"

                    //$('p').filter(function (index)
                    //{
                    //    if (index === 0 || index === 2)
                    //    {
                    //        return true;
                    //    } else
                    //    {
                    //        return false;
                    //    }
                    //}).css('border', '3px solid black'); // выбирает элементы <p/> с индексом 0 и 2

                    //Поиск в наборе
                    //$('#divBlock').children().css('border', '3px solid black');// выбирает все дочерние элементы в элементе с id="divBlock"
                    //$('#divBlock').children('a').css('border', '3px solid black');// выбирает все дочерние элементы <a/> в элементе с id="divBlock"
                    //$('body > *').parent().css('border', '3px solid black');
                    //$('#middle').next().css('border', '3px solid black'); // выбирает элемент следующий за элементом с id="middle"
                    //$('#middle').nextAll().css('border', '3px solid black'); // выбирает все элементы следующие за элементом с id="middle"
                    //$('#middle').siblings().css('border', '3px solid black');// выбирает все элементы вокруг элемента с id="middle"
                    //$('#middle').prev().css('border', '3px solid black'); // выбирает предыдущий элемент перед элементом с id="middle"
                    //$('#divBlock').prevAll().css('border', '3px solid black'); // выбирает все предыдущие элементы перед элементом с id="divBlock"
                    //$('#middle').nextAll().css('border', '3px solid black'); // выбирает все элементы следующие за элементом с id="middle"

                    //Манипуляции набором элементов
                    //$('#divBlock > span').add('p[class]').css('border', '3px solid black'); // выбирает дочерние элементы <span/> в элементе с id="divBlock" + все элементы <p/> с атрибутом class
                    //$('#divBlock > span').closest('div').css('border', '3px solid black'); // выбирает
                    //$('body > *').contents().css('border', '3px solid black'); // выбирает в дочерних элементах <body/> их дочерние элементы если есть
                    //$('body > *').contents('span').css('border', '3px solid black'); // выбирает в дочерних элементах <body/> их дочерние элементы <span/> если есть
                    //$('p').find('*[id]').css('border', '3px solid black'); // выбирает

                    //Атрибуты
                    //Windows.UI.Popups.MessageDialog($('p').attr('id')).showAsync(); //вернет значение атрибута id первого элемента <p/>
                    //$('div span').attr('id', 'span_in_div'); //устанавливает атрибут id="span_in_div" ко всем элементам span во всех блоках div
                    //$('div span').attr('id', function (index)
                    //{
                    //    return 'span_in_div_' + index;
                    //}); //устанавливает уникальный атрибут id="span_in_div_+index" ко всем элементам span во всех блоках div
                    //$(divBlock).hide(); // прячет указанный элемент (show() показывает)

                    //Связывание событий с обработчиками
                    //$('#divBlock').bind('click', clickHandler);
                    //$('#divBlock').one('click', clickHandler);



                });


            } else
            {
                // TODO: это приложение было активировано из приостановленного состояния.
                // Место для состояния восстановления приложения.
            }
            args.setPromise(WinJS.UI.processAll().then(function ()
            {
                // TODO: место для кода.
            }));
        }
    };
    app.oncheckpoint = function (args)
    {
        // TODO: действие приложения будет приостановлено. Сохраните здесь все состояния, которые понадобятся после приостановки.
        // Вы можете использовать объект WinJS.Application.sessionState, который автоматически сохраняется и восстанавливается после приостановки.
        // Если вам нужно завершить асинхронную операцию до того, как действие приложения будет приостановлено, вызовите args.setPromise().
    };
    app.start();
}());

function clickHandler(eventObj)
{
    $('#divBlock').toggleClass('s5');
}
